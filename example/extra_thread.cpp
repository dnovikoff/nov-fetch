// Call this application as extra_thread <url1> <url2> ...

#include <iostream>
#include <string>
#include <thread>

#include <nov/fetch/fetcher.hpp>
#include <nov/fetch/handler.hpp>

class DebugHandler: public Nov::Fetch::Handler {
public:
	explicit DebugHandler(const std::string& u): url(u) {
	}

	void onStart(int statusCode) override {
		status = statusCode;
	}

	void onHeader(const std::string&) override {
	}

	void onWrite(const std::string&) override {
	}

	void onDone() override {
		std::cout << url << " done " << status << std::endl;
	}

	void onFail(const std::string& error) override {
		std::cout << url << " failed with error " << error << std::endl;
	}
private:
	int status = 0;
	std::string url;
};

int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Should have at least one arg" << std::endl;
		return 1;
	}
	Nov::Fetch::Fetcher f;

	for (int i=1; i < argc; ++i) {
		Nov::Fetch::Request r;
		r.timeoutMs = 1000;
		r.url = argv[i];
		r.handler.reset(new DebugHandler(r.url));
		std::cout << "Requested " << r.url << std::endl;
		f.fetch(std::move(r));
	}

	std::thread t([&f] {
		f.run(Nov::Fetch::RunMode::ExitWhenDone);
	});

	t.join();
	return 0;
}
