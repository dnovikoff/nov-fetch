// Call this application as progress <url> > my.txt

#include <cstdlib>
#include <iostream>
#include <string>

#include <nov/fetch/fetcher.hpp>
#include <nov/fetch/handler.hpp>

static std::string contentLength("Content-Length: ");

class DebugHandler: public Nov::Fetch::Handler {
public:
	void onStart(int) override {
	}

	void onHeader(const std::string& header) override {
		if (header.find(contentLength) == std::string::npos) {
			return;
		}
		bytesTotal = std::atol(header.c_str() + contentLength.size());
		std::cout << ">> " << header << std::endl;
	}

	void onWrite(const std::string& part) override {
		bytesRead += part.size();
		printStatus();
		std::cout << part << std::flush;
	}

	void onDone() override {
		std::cerr << std::endl;
	}

	void onFail(const std::string& error) override {
		isError = true;
		std::cerr << "\n>> Error: " << error << std::endl;
	}

	void printStatus() {
		std::cerr << "\rProgress [";
		if (bytesTotal == 0) {
			std::cerr << "Unknown";
		} else {
			const size_t doneLen = bytesRead * progressLength / bytesTotal;
			std::cerr << std::string(doneLen, '=');
			if (doneLen < progressLength) {
				std::cerr << ">" << std::string(progressLength-doneLen-1, ' ');
			}
		}
		std::cerr << "] " << bytesRead << "/" << bytesTotal << "                 ";
	}

private:
	static const size_t progressLength = 40;
	size_t bytesTotal = 0;
	size_t bytesRead = 0;
	bool isError = false;
};

void request(const std::string& url) {
	Nov::Fetch::Fetcher f;
	Nov::Fetch::Request r;
	r.timeoutIdleMs = 10000;
	r.url = url;
	r.handler.reset(new DebugHandler);
	f.fetch(std::move(r));
	f.run(Nov::Fetch::RunMode::ExitWhenDone);
}

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cerr << "Should be exactly one argument - link to get" << std::endl;
		return 1;
	}
	request(argv[1]);
	return 0;
}
