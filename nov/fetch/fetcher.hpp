#ifndef NOV_FETCH_FETCHER_HPP_
#define NOV_FETCH_FETCHER_HPP_

#include <memory>

#include <nov/fetch/request.hpp>
#include <nov/fetch/run_mode.hpp>

namespace Nov {
namespace Fetch {

namespace Internal {
class FetcherImpl;
} // namespace Internal

class Fetcher final {
public:
	 Fetcher();
	~Fetcher();

	Fetcher(Fetcher&&);
	Fetcher& operator=(Fetcher&&);

	void fetch(Request&& request);

	void stop();

	void run(RunMode mode);

private:
	std::unique_ptr<Internal::FetcherImpl> impl_;
};

} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_FETCHER_HPP_
