#ifndef NOV_FETCH_REQUEST_HPP_
#define NOV_FETCH_REQUEST_HPP_

#include <string>
#include <vector>
#include <memory>

#include <nov/fetch/handler.hpp>
#include <nov/fetch/method.hpp>

namespace Nov {
namespace Fetch {

struct Request {
	typedef std::unique_ptr<Handler> HandlerPtr;
	typedef std::vector<std::string> Headers;

	std::string url;
	Headers headers;
	Method method = Method::Get;
	HandlerPtr handler;
	size_t timeoutMs = 0;
	size_t timeoutIdleMs = 0;
	bool supportAllEncodings = true;

	Request() {
	}

	Request(Request&&) = default;
	Request& operator=(Request&&) = default;
private:
	Request(const Request&) = delete;
	Request& operator=(const Request&) = delete;
};

} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_REQUEST_HPP_
