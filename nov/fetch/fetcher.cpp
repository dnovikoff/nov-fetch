#include "fetcher.hpp"
#include "internal/fetcher_impl.hpp"

namespace Nov {
namespace Fetch {

void Fetcher::fetch(Request&& request) {
	impl_->fetch(std::move(request));
}

void Fetcher::stop() {
	impl_->stop();
}

void Fetcher::run(RunMode mode) {
	impl_->run(mode);
}

Fetcher::Fetcher(): impl_(new Internal::FetcherImpl) {
}

Fetcher::~Fetcher() {
}

Fetcher::Fetcher(Fetcher&&) = default;
Fetcher& Fetcher::operator=(Fetcher&&) = default;

} // namespace Fetch
} // namespace Nov
