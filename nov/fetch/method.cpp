#include "method.hpp"

namespace Nov {
namespace Fetch {

#define CASE(ENUM,STR) case (Method::ENUM): return STR;

const char * methodString(const Method m) {
	switch (m) {
		CASE(Options, "OPTIONS")
		CASE(Get, "GET")
		CASE(Head, "HEAD")
		CASE(Post, "POST")
		CASE(Put, "PUT")
		CASE(Delete, "DELETE")
		CASE(Trace, "TRACE")
		CASE(Connect, "CONNECT")
		default:
			break;
	}
	return nullptr;
}

} // namespace Fetch
} // namespace Nov
