#ifndef NOV_FETCH_RUN_MODE_HPP_
#define NOV_FETCH_RUN_MODE_HPP_

namespace Nov {
namespace Fetch {

enum class RunMode {
	ExitWhenDone,
	WorkUntilStopped
};

} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_RUN_MODE_HPP_
