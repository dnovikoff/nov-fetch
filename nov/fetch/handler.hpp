#ifndef NOV_FETCH_HANDLER_HPP_
#define NOV_FETCH_HANDLER_HPP_

#include <string>

namespace Nov {
namespace Fetch {

class Handler {
public:
	Handler() {}

	// Called before any other requests
	virtual void onStart(int httpCode) = 0;
	// Called when new header received
	// All headers will be received before any call to onWrite
	virtual void onHeader(const std::string& header) = 0;

	// Called when data received. Could be called multiple times
	virtual void onWrite(const std::string& part) = 0;

	// Called at the parse end in case of success
	virtual void onDone() = 0;

	// Called in case of fail. Ex.: timeout
	virtual void onFail(const std::string& error) = 0;

	virtual ~Handler() {
	}
private:
	Handler(const Handler&) = delete;
	Handler& operator=(const Handler&) = delete;
};

} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_HANDLER_HPP_
