#include "multi.hpp"
#include "error.hpp"

#include <cassert>

namespace Nov {
namespace Fetch {
namespace Internal {

void Multi::init() {
	if (!multi_) {
		multi_ = curl_multi_init();
		initCallback_(multi_);
	}
}

Multi::Multi(InitCallback initCallback): initCallback_(initCallback) {
	init();
}

Multi::~Multi() {
	clean();
}

void Multi::clean() {
	if (!multi_) {
		return;
	}
	auto req = std::move(requests_);
	for (const auto& x: req) {
		exitIfError(__FILE__, __LINE__,
			curl_multi_remove_handle(multi_, x.first)
		);
	}
	exitIfError(__FILE__, __LINE__,
		curl_multi_cleanup(multi_)
	);
	multi_ = nullptr;
}

CURLM* Multi::handle() {
	if (!multi_) {
		exitWithError(__FILE__, __LINE__, "ERROR: multi not initialized");
	}
	return multi_;
}

Data* Multi::get(CURL* easy) {
	auto it = requests_.find(easy);
	if (it == requests_.end()) {
		return nullptr;
	}
	return it->second.get();
}

std::unique_ptr<Data> Multi::remove(CURL* easy) {
	auto i = requests_.find(easy);
	curl_multi_remove_handle(multi_, easy);
	auto r = std::move(i->second);
	requests_.erase(easy);
	return r;
}

void Multi::add(std::unique_ptr<Data>&& data) {
	auto easy = data->context.get();
	exitIfError(__FILE__, __LINE__,
		curl_multi_add_handle(multi_, easy)
	);

	requests_[easy] = std::move(data);
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
