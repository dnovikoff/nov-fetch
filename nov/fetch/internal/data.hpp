#ifndef NOV_FETCH_INTERNAL_DATA_HPP_
#define NOV_FETCH_INTERNAL_DATA_HPP_

#include <curl/curl.h>

#include <memory>

#include <nov/fetch/request.hpp>
#include <nov/fetch/internal/headers.hpp>
#include <nov/fetch/internal/connection.hpp>

namespace Nov {
namespace Fetch {
namespace Internal {

class ContextFree {
public:
	void operator()(CURL* easy) const;
};

typedef std::unique_ptr<CURL, ContextFree> EasyContextHolder;

struct Data final {
	Headers curlHeaders;
	Request request;
	EasyContextHolder context;
	std::unique_ptr<Connection> connection;
	int statusCode = 0;

	bool initContext();

	Data() = default;
	Data(const Data&) = delete;
	Data& operator=(const Data&) = delete;

	~Data();
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_DATA_HPP_
