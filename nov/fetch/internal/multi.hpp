#ifndef NOV_FETCH_INTERNAL_MULTI_HPP_
#define NOV_FETCH_INTERNAL_MULTI_HPP_

#include <map>
#include <curl/curl.h>
#include <nov/fetch/internal/data.hpp>

namespace Nov {
namespace Fetch {
namespace Internal {

class Multi {
public:
	typedef std::function<void(CURLM*)> InitCallback;
	explicit Multi(InitCallback initCallback);
	~Multi();

	Data* get(CURL* easy);
	void add(std::unique_ptr<Data>&& data);
	std::unique_ptr<Data> remove(CURL* easy);

	CURLM* handle();

private:
	void clean();
	void init();
	Multi(const Multi&) = delete;
	Multi& operator=(const Multi&) = delete;

	typedef std::map<CURL*, std::unique_ptr<Data> > Requests;

	CURLM* multi_ = nullptr;
	Requests requests_;
	InitCallback initCallback_;
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_MULTI_HPP_
