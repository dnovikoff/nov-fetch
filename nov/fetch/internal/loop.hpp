#ifndef NOV_FETCH_INTERNAL_LOOP_HPP_
#define NOV_FETCH_INTERNAL_LOOP_HPP_

#include <curl/curl.h>
#include <ev++.h>

#include <memory>
#include <atomic>

#include <nov/fetch/run_mode.hpp>

namespace Nov {
namespace Fetch {
namespace Internal {

class FetcherImpl;
class Connection;

class Loop final {
public:
	explicit Loop(FetcherImpl* fetcher);
	~Loop();

	Loop(const Loop&) = delete;
	Loop& operator=(const Loop&) = delete;

	std::unique_ptr<Connection> connect(curl_socket_t fd);

	void setTimeoutMs(size_t ms);
	void onTimeout();
	void onNotify();
	void stopTimer();

	void stop();
	void run();
	void work();

	void notify();

private:
	friend class Connection;
	friend class FetcherImpl;

	ev::dynamic_loop loop_;
	ev::timer timer_;
	ev::async notify_;
	FetcherImpl* fetcher_ = nullptr;
	std::atomic_bool stopped_;
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_LOOP_HPP_
