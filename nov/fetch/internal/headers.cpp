#include "headers.hpp"

#include <curl/curl.h>

namespace Nov {
namespace Fetch {
namespace Internal {

Headers::Headers() {
}

Headers::~Headers() {
	if (!context) {
		return;
	}
	curl_slist_free_all(context);
}

void Headers::add(const std::string& line) {
	context = curl_slist_append(context, line.c_str());
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
