#include "error.hpp"

#include <iostream>

namespace Nov {
namespace Fetch {
namespace Internal {

void exitWithError(const char * file, int line, const char * error) {
	std::cerr << file << ": " << line << " curl_multi ERROR: " << error << std::endl;
	exit(1);
}

void exitIfError(const char * file, int line, CURLMcode code) {
	if (CURLM_OK == code) {
		return;
	}
	exitWithError(file, line, curl_multi_strerror(code));
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
