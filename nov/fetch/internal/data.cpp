#include "data.hpp"

#include <curl/curl.h>

namespace Nov {
namespace Fetch {
namespace Internal {

namespace {
class GlobalContext {
public:
	GlobalContext() {
		if (CURLE_OK != curl_global_init(CURL_GLOBAL_DEFAULT)) {
			return;
		}
		initialized = true;
	}

	~GlobalContext() {
		if (!initialized) {
			return;
		}
		curl_global_cleanup();
	}

	EasyContextHolder newContext() {
		if (!initialized) {
			return nullptr;
		}
		return EasyContextHolder(curl_easy_init());
	}
private:
	bool initialized = false;

	GlobalContext(const GlobalContext&) = delete;
	GlobalContext& operator=(const GlobalContext&) = delete;
} globalContext;
} // namespace

bool Data::initContext() {
	context = globalContext.newContext();
	return context.get();
}

EasyContextHolder newEasy() {
	return globalContext.newContext();
}

void ContextFree::operator()(CURL* easy) const {
	if (!easy) {
		return;
	}
	curl_easy_cleanup(easy);
}

Data::~Data() {
	request.handler.reset();
	connection.reset();
	context.reset();
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
