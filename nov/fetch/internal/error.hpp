#ifndef NOV_FETCH_INTERNAL_ERROR_HPP_
#define NOV_FETCH_INTERNAL_ERROR_HPP_

#include <curl/curl.h>

namespace Nov {
namespace Fetch {
namespace Internal {

extern void exitWithError(const char * file, int line, const char * error);
extern void exitIfError(const char * file, int line, CURLMcode code);

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_ERROR_HPP_
