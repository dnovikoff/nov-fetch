#ifndef NOV_FETCH_INTERNAL_FETCHER_IMPL_HPP_
#define NOV_FETCH_INTERNAL_FETCHER_IMPL_HPP_

#include <ev++.h>

#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <queue>

#include <nov/fetch/request.hpp>
#include <nov/fetch/run_mode.hpp>
#include <nov/fetch/internal/multi.hpp>
#include <nov/fetch/internal/loop.hpp>

namespace Nov {
namespace Fetch {
namespace Internal {

class FetcherImpl {
public:
	FetcherImpl();
	virtual ~FetcherImpl();

	void onSocket(CURL* e, curl_socket_t fd, int what);
	void onSocket(Connection* info, int what);

	// Will block
	void run(RunMode mode);

	void startPending();
	// To be called from other thread
	void fetch(Request&& request);
	void stop();

private:
	friend class Loop;
	friend class Connection;

	std::queue<Request> moveQueue();

	void startRequest(Request&& request);
	void onTimeout();
	void processMessages();
	void onIoEvent(Connection* connection, int flags);

	Loop loop_;
	Multi multi_;

	// Should not be serialized, cause used from one thread only
	int inProgressCount_ = 0;

	std::mutex queueMutex_;
	std::queue<Request> queue_;
	RunMode mode_;
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_FETCHER_IMPL_HPP_
