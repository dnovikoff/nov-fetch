#include "connection.hpp"
#include "fetcher_impl.hpp"

namespace Nov {
namespace Fetch {
namespace Internal {

static void staticOnPerform(struct ev_loop*, ev_io *handle, int events) {
	static_cast<Connection*>(handle->data)->perform(events);
}

Connection::Connection(FetcherImpl* fetcher, curl_socket_t fd) {
	descriptor_ = fd;
	fetcher_ = fetcher;
	io_.loop = fetcher->loop_.loop_;
	io_.data = this;
	io_.cb = staticOnPerform;
	io_.fd = fd;
}

Connection::~Connection() {
}

bool Connection::close() {
	io_.stop();
	return true;
}

void Connection::perform(int events) {
	int flags = 0;
	if (events & EV_READ) {
		flags |= CURL_CSELECT_IN;
	}
	if (events & EV_WRITE) {
		flags |= CURL_CSELECT_OUT;
	}

	fetcher_->onIoEvent(this, flags);
}

void Connection::read() {
	event(EV_READ);
}

void Connection::write() {
	event(EV_WRITE);
}

void Connection::readWrite() {
	event(EV_READ | EV_WRITE);
}

void Connection::event(int flags) {
	io_.start(descriptor_, flags);
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
