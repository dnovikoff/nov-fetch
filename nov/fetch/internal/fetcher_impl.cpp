#include "fetcher_impl.hpp"
#include "error.hpp"

#include <assert.h>
#include <curl/curl.h>

static_assert(EV_READ == CURL_CSELECT_IN, "EV_READ != CURL_CSELECT_IN");
static_assert(EV_WRITE == CURL_CSELECT_OUT, "EV_WRITE != CURL_CSELECT_OUT");

namespace Nov {
namespace Fetch {
namespace Internal {

static void setCode(Data* obj) {
	if (obj->statusCode != 0) {
		return;
	}

	long code = 0;
	curl_easy_getinfo(obj->context.get(), CURLINFO_RESPONSE_CODE, &code);
	if (code == 0) {
		return;
	}
	obj->statusCode = static_cast<int>(code);
	obj->request.handler->onStart(obj->statusCode);
}

static size_t curlWriteCallback(char* data, size_t symbolSize, size_t symbolCount, Data* obj) {
	setCode(obj);

	const size_t size = symbolSize*symbolCount;
	obj->request.handler->onWrite(std::string(data, size));
	return size;
}

static size_t curlHeaderCallback(char* data, size_t symbolSize, size_t symbolCount, Data* obj) {
	setCode(obj);

	const size_t size = symbolSize*symbolCount;
	std::string header(data, size);
	header.resize(header.find_last_not_of("\r\n") + 1);
	obj->request.handler->onHeader(header);
	return size;
}

static void setLong(void *context, CURLoption option, long value) {
	curl_easy_setopt(context, option, value);
}

static void setBool(void *context, CURLoption option, bool value) {
	setLong(context, option, value?1:0);
}

static void setTrue(void *context, CURLoption option) {
	setBool(context, option, true);
}

static long msToSec(long ms) {
	return (ms / 1000) + (((ms % 1000) > 0)?1:0);
}

void FetcherImpl::startRequest(Request&& request) {
	std::unique_ptr<Data> data(new Data);
	if (!data->initContext()) {
		request.handler->onFail("Could not initialize request");
	}
	data->request = std::move(request);

	++inProgressCount_;

	const auto easy = data->context.get();

	// Disable dns caching
	setLong(easy, CURLOPT_DNS_CACHE_TIMEOUT, 0);
	curl_easy_setopt(easy, CURLOPT_WRITEFUNCTION, curlWriteCallback);
	curl_easy_setopt(easy, CURLOPT_HEADERFUNCTION, curlHeaderCallback);

//	setBool(easy,  CURLOPT_NOSIGNAL, true);

	curl_easy_setopt(easy, CURLOPT_URL, data->request.url.c_str());
	const long timeoutMs = static_cast<long>(request.timeoutMs);
	if (timeoutMs > 0) {
		setLong(easy, CURLOPT_TIMEOUT_MS, timeoutMs);
	}
	const long timeoutIdleMs = static_cast<long>(request.timeoutIdleMs);
	if (timeoutIdleMs > 0) {
		const long timeoutIdleSec = msToSec(timeoutIdleMs);
		setLong(easy, CURLOPT_LOW_SPEED_TIME, timeoutIdleSec);
		setLong(easy, CURLOPT_LOW_SPEED_LIMIT, 10);
	}

	if (data->request.supportAllEncodings) {
		curl_easy_setopt(easy, CURLOPT_ACCEPT_ENCODING, "");
	}

	const auto method = data->request.method;
	switch(data->request.method) {
	case (Method::Get):
		// Do nothing
		break;
	case (Method::Head):
		setTrue(easy, CURLOPT_NOBODY);
		curl_easy_setopt(easy, CURLOPT_NOBODY, methodString(method));
		break;
	case (Method::Post):
		setTrue(easy, CURLOPT_POST);
		//TODO:
		break;
//	case (Method::Put):
//		break;
	default:
		curl_easy_setopt(easy, CURLOPT_CUSTOMREQUEST, methodString(method));
		break;
	}

	const auto& headers = data->request.headers;
	if (!headers.empty()) {
		Headers& curlHeaders = data->curlHeaders;
		for (const auto& header: headers) {
			curlHeaders.add(header);
		}
		curl_easy_setopt(easy, CURLOPT_HTTPHEADER, curlHeaders.context);
	}

	auto ptr = data.get();

	curl_easy_setopt(easy, CURLOPT_WRITEDATA, ptr);
	curl_easy_setopt(easy, CURLOPT_HEADERDATA, ptr);

	multi_.add(std::move(data));
}

void FetcherImpl::processMessages() {
	CURLMsg * msg = nullptr;
	int unused = 0;
	while ((msg = curl_multi_info_read(multi_.handle(), &unused))) {
		if (msg->msg != CURLMSG_DONE) {
			continue;
		}
		const CURLcode code = msg->data.result;
		CURL* const easy = msg->easy_handle;
		auto data = multi_.remove(easy);

		auto handler = data->request.handler.get();
		if (code == CURLE_OK) {
			setCode(data.get());
			handler->onDone();
		} else {
			handler->onFail((curl_easy_strerror(code)));
		}
	}
	if (inProgressCount_ == 0) {
		if (mode_ == RunMode::ExitWhenDone) {
			loop_.stop();
		} else {
			loop_.stopTimer();
		}
	}
}

static int multiSocket(CURL* e, curl_socket_t s, int what, FetcherImpl* ths, Connection* connection) {
	if (what == CURL_POLL_REMOVE) {
		connection->close();
	} else if (connection == nullptr) {
		ths->onSocket(e, s, what);
	} else {
		ths->onSocket(connection, what);
	}
	return 0;
}

void FetcherImpl::onIoEvent(Connection* connection, int flags) {
	exitIfError(__FILE__, __LINE__,
		curl_multi_socket_action(multi_.handle(), connection->descriptor_, flags, &inProgressCount_)
	);
	processMessages();
}

void FetcherImpl::onSocket(Connection* connection, int what) {
	switch(what) {
	case CURL_POLL_IN:
		connection->read();
		break;
	case CURL_POLL_OUT:
		connection->write();
		break;
	case CURL_POLL_INOUT:
		connection->readWrite();
		break;
	}
}

void FetcherImpl::onSocket(CURL* e, curl_socket_t fd, int what) {
	auto data = multi_.get(e);
	if (!data) {
		// could not be it
		return;
	}
	data->connection = loop_.connect(fd);
	auto ptr = data->connection.get();
	ptr->curl_ = e;

	exitIfError(__FILE__, __LINE__,
		curl_multi_assign(multi_.handle(), fd, ptr)
	);

	onSocket(ptr, what);

}

static int multiTimer(CURLM*, long timeoutMs, Loop* loop) {
	if (timeoutMs >= 0) {
		loop->setTimeoutMs(timeoutMs);
	}
	return 0;
}

void FetcherImpl::onTimeout() {
	exitIfError(__FILE__, __LINE__,
		curl_multi_socket_action(multi_.handle(), CURL_SOCKET_TIMEOUT, 0, &inProgressCount_)
	);
	processMessages();
}

FetcherImpl::FetcherImpl(): loop_(this),
	multi_([this] (CURLM* handle) {
		curl_multi_setopt(handle, CURLMOPT_SOCKETFUNCTION, multiSocket);
		curl_multi_setopt(handle, CURLMOPT_SOCKETDATA, this);

		curl_multi_setopt(handle, CURLMOPT_TIMERFUNCTION, multiTimer);
		curl_multi_setopt(handle, CURLMOPT_TIMERDATA, &loop_);
	})
{
}

FetcherImpl::~FetcherImpl() {
}

void FetcherImpl::fetch(Request&& request) {
	std::lock_guard<std::mutex> l(queueMutex_);
	queue_.push(std::move(request));
	loop_.notify();
}
std::queue<Request> FetcherImpl::moveQueue() {
	std::lock_guard<std::mutex> l(queueMutex_);
	return std::move(queue_);
}

void FetcherImpl::startPending() {
	auto q = moveQueue();
	while (!q.empty()) {
		startRequest(std::move(q.front()));
		q.pop();
	}
}

void FetcherImpl::stop() {
	loop_.stop();
}

void FetcherImpl::run(RunMode mode) {
	mode_ = mode;
	loop_.run();
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
