#include "loop.hpp"
#include "fetcher_impl.hpp"

namespace Nov {
namespace Fetch {
namespace Internal {

static void staticOnNotify(struct ev_loop*, ev_async *handle, int) {
	static_cast<Loop*>(handle->data)->onNotify();
}

static void staticOnTimeout(struct ev_loop*, ev_timer *handle, int) {
	static_cast<Loop*>(handle->data)->onTimeout();
}

void Loop::onNotify() {
	if (stopped_) {
		stopTimer();
		loop_.break_loop();
		return;
	}
	fetcher_->startPending();
}

Loop::Loop(FetcherImpl* fetcher): fetcher_(fetcher) {
	timer_.data = this;
	stopped_ = true;

	notify_.cb = staticOnNotify;
	timer_.cb = staticOnTimeout;

	notify_.loop = loop_;
	timer_.loop = loop_;

	notify_.data = this;
	timer_.data = this;

	notify_.start();
}

void Loop::stop() {
	if (!stopped_) {
		stopped_ = true;
		notify();
	}
}

void Loop::notify() {
	notify_.send();
}

void Loop::run() {
	if (!stopped_) {
		throw std::logic_error("Already running loop");
	}
	stopped_ = false;

	loop_.run();
}

Loop::~Loop() {
}

void Loop::setTimeoutMs(size_t ms) {
	if (ms == 0) {
		ms = 1;
	}
	double d = ms;
	d /= 1000;
	timer_.start(d, .0);
}

void Loop::stopTimer() {
	timer_.stop();
}

void Loop::onTimeout() {
	fetcher_->onTimeout();
}

std::unique_ptr<Connection> Loop::connect(curl_socket_t fd) {
	return std::unique_ptr<Connection>(new Connection(fetcher_, fd));
}

} // namespace Internal
} // namespace Fetch
} // namespace Nov
