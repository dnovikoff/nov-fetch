#ifndef NOV_FETCH_INTERNAL_HEADERS_HPP_
#define NOV_FETCH_INTERNAL_HEADERS_HPP_

#include <string>

struct curl_slist;

namespace Nov {
namespace Fetch {
namespace Internal {

// Hold values for curl slits
class Headers {
public:
	Headers();
	~Headers();
	void add(const std::string& line);
private:
	friend class FetcherImpl;

	Headers(const Headers&) = delete;
	Headers& operator=(const Headers&) = delete;

	curl_slist* context = nullptr;
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_HEADERS_HPP_
