#ifndef NOV_FETCH_INTERNAL_CONNECTION_HPP_
#define NOV_FETCH_INTERNAL_CONNECTION_HPP_

#include <ev++.h>
#include <curl/curl.h>

namespace Nov {
namespace Fetch {
namespace Internal {

class Loop;
class FetcherImpl;

class Connection final {
public:
	~Connection();

	void read();
	void write();
	void readWrite();

	void perform(int events);

	void setFetcher(FetcherImpl* f) {
		fetcher_ = f;
	}
	bool close();

private:
	friend class Loop;
	friend class FetcherImpl;

	explicit Connection(FetcherImpl* fetcher, curl_socket_t fd);

	void event(int flags);

	curl_socket_t descriptor_ = 0;
	ev::io io_;
	FetcherImpl* fetcher_ = nullptr;
	CURL* curl_ = nullptr;
};

} // namespace Internal
} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_INTERNAL_CONNECTION_HPP_
