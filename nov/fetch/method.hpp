#ifndef NOV_FETCH_METHOD_HPP_
#define NOV_FETCH_METHOD_HPP_

namespace Nov {
namespace Fetch {

enum class Method {
	Options=1,
	Get,
	Head,
	Post,
	Put,
	Delete,
	Trace,
	Connect,
	Patch,
	Purge,
	Error,
};

const char * methodString(const Method m);

} // namespace Fetch
} // namespace Nov

#endif // NOV_FETCH_METHOD_HPP_
