# nov-fetcher lib #

This is a library to fetch multiple documents in **async** mode. The implementation uses **Curl** library to fetch documents and **libev** for managing **epoll**.


### Minimum example ###

```
#!c++

#include <iostream>
#include <string>

#include <nov/fetch/fetcher.hpp>
#include <nov/fetch/handler.hpp>

class DebugHandler: public Nov::Fetch::Handler {
public:
	void onStart(int statusCode) override {
		std::cout << "#debug start " << statusCode << std::endl;
	}

	void onHeader(const std::string& header) override {
		std::cout << "#debug header " << header << std::endl;
	}

	void onWrite(const std::string& part) override {
		std::cout << "#debug write " << part << std::endl;
	}

	void onDone() override {
		std::cout << "#debug done " << std::endl;
	}

	void onFail(const std::string& error) override {
		std::cout << "#debug fail " << error << std::endl;
	}
};

void request(const std::string& url) {
	Nov::Fetch::Fetcher f;
	Nov::Fetch::Request r;
	r.timeoutIdleMs = 10000;
	r.url = url;
	r.handler.reset(new DebugHandler);
	f.fetch(std::move(r));
	f.run(Nov::Fetch::RunMode::ExitWhenDone);
}

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cerr << "Should be exactly one argument - link to get" << std::endl;
		return 1;
	}
	request(argv[1]);
	return 0;
}


```

See **example** dir for more examples