#include "../test.hpp"

#include <nov/fetch/fetcher.hpp>

using namespace Nov::Fetch;

struct ResultData {
	std::string requestedUrl{"!!!!"};
	std::string data;
	std::ostringstream errors;
	std::vector<std::string> headers;
	int httpCode = 0;
};

class TestHandler: public Handler {
public:
	enum State {
		Nothing,
		Started,
		Headers,
		Write,
		Done,
		Fail,
	};
	explicit TestHandler(ResultData& d):resultData(d) {
	}

	void onStart(int httpCode) override {
		std::cout << "#debug start " << resultData.requestedUrl << " " << httpCode << std::endl;
		if (state != Nothing) {
			resultData.errors << "Wrong state " << state << " while starting\n";
		}
		resultData.httpCode = httpCode;
		state = Started;
	}

	void onHeader(const std::string& header) override {
		std::cout << "#debug header " << resultData.requestedUrl << " " << header << std::endl;
		if (state > Headers) {
			resultData.errors << "Wrong state " << state << " while reading headers\n";
		}
		state = Headers;
		resultData.headers.push_back(header);
	}

	void onWrite(const std::string& part) override {
		std::cout << "#debug write " << resultData.requestedUrl << " " << part.size() << std::endl;
//		std::cout << part << std::endl;
		if (state > Write) {
			resultData.errors << "Wrong state " << state << " while reading data\n";
		}
		state = Write;
		resultData.data.append(part);
	}

	void onDone() override {
		std::cout << "#debug done " << resultData.requestedUrl << std::endl;
		if (state > Write || state == Nothing) {
			resultData.errors << "Wrong state " << state << " while on done\n";
		}
		state = Done;
	}

	void onFail(const std::string& error) override {
		std::cout << "#debug fail " << resultData.requestedUrl << std::endl;
		resultData.data = error;
		if (state > Write) {
			resultData.errors << "Wrong state " << state << " while on fail\n";
		}
		state = Done;
		resultData.httpCode = -1;
	}

private:
	ResultData& resultData;
	State state = Nothing;
};

class Tester;

class Result {
	friend class Tester;
public:
	bool check(int expectedCode, const std::string& expectedSubstring) {
		bool result = waitResult();
		if (expectedCode != data->httpCode) {
			std::cout << "#error for http code expected=" << expectedCode << " got=" << data->httpCode << std::endl;
			result = false;
		}
		if (data->data.find(expectedSubstring) == std::string::npos) {
			std::cout << "#error response to have substring expected=" << expectedSubstring << " got=" << data->data << std::endl;
			result = false;
		}
		return result;
	}
	bool checkExact(int expectedCode, const std::string& expected) {
		bool result = waitResult();
		if (expectedCode != data->httpCode) {
			std::cout << "#error for http code expected=" << expectedCode << " got=" << data->httpCode << std::endl;
			result = false;
		}
		if (data->data != expected) {
			std::cout << "#error response to have substring expected=" << expected << " got=" << data->data << std::endl;
			result = false;
		}
		return result;
	}
	std::string fail() {
		std::string result;
		if (!waitResult()) {
			result.assign("Unexpected errors");
		} else if (data->httpCode != -1) {
			result.assign("Expected to fail");
		} else {
			result.swap(data->data);
		}
		return result;
	}

private:
	bool waitResult();
	Result(Tester& f, Request req);

	std::unique_ptr<ResultData> data;
	Tester& tester;
};

class Tester {
	friend class Result;
public:
	Tester() {
	}
	~Tester() {
	}
	Result get(const std::string& url) {
		Request req;
		req.url = url;
		done_ = false;
		return Result(*this, std::move(req));
	}
	Result get(const std::string& url, Request::Headers&& headers) {
		Request req;
		req.url = url;
		req.headers = std::move(headers);
		done_ = false;
		return Result(*this, std::move(req));
	}
	Result head(const std::string& url) {
		Request req;
		req.url = url;
		req.method = Method::Head;
		done_ = false;
		return Result(*this, std::move(req));
	}

	void run() {
		if (done_) {
			return;
		}
		fetcher_.run(Nov::Fetch::RunMode::ExitWhenDone);
		done_ = true;
	}

private:
	bool done_ = false;
	Fetcher fetcher_;
};


bool Result::waitResult() {
	tester.run();

	const std::string error = data->errors.str();
	if (!error.empty()) {
		std::cout << "Errors: " << error << std::flush;
		return false;
	}
	return true;
}

Result::Result(Tester& f, Request req):tester(f) {
	std::cout << "#debug url=" << req.url << std::endl;
	data.reset(new ResultData);
	data->requestedUrl = "'" + req.url + "'";
	req.timeoutIdleMs = 30000;
	req.supportAllEncodings = true;
	req.handler = std::unique_ptr<TestHandler>(new TestHandler(*data));
	tester.fetcher_.fetch(std::move(req));
}

BOOST_AUTO_TEST_CASE( result200 ) {
	Tester t;
	BOOST_CHECK(t.get("http://example.com").check(200, "Example Domain"));
}

BOOST_AUTO_TEST_CASE( result200_1 ) {
	Tester t;
	BOOST_CHECK(t.get("http://www.example1.com/").check(200, "From The Collaborative International Dictionary of English"));
}

BOOST_AUTO_TEST_CASE( result404 ) {
	Tester t;
	BOOST_CHECK(t.get("http://exmaple.com/no_such_page").check(404, "404 Not Found"));
}

BOOST_AUTO_TEST_CASE( incorrectUrl ) {
	Tester t;
	BOOST_CHECK_EQUAL(t.get("not an url").fail(), "Couldn't resolve host name");
}

BOOST_AUTO_TEST_CASE( incorrectDomain ) {
	Tester t;
	BOOST_CHECK_EQUAL(t.get("http://no.such.domain").fail(), "Couldn't resolve host name");
}

BOOST_AUTO_TEST_CASE( sleepAndRepeat ) {
	Tester t;
	BOOST_CHECK(t.get("http://example.com").check(200, "Example Domain"));
	// will sleep here
	BOOST_CHECK(t.get("http://www.example1.com/").check(200, "From The Collaborative International Dictionary of English"));
}

BOOST_AUTO_TEST_CASE( headMethod ) {
	Tester t;
	BOOST_CHECK(t.head("http://example.com").checkExact(200, ""));
}

BOOST_AUTO_TEST_CASE( multipleRequests ) {
	Tester t;
	auto r200 = t.get("http://example.com");
	auto r200_1 = t.get("http://www.example1.com/");
	auto r404 = t.get("http://exmaple.com/no_such_page");

	auto rWrongUrl = t.get("not an url");
	auto rWrongDomain = t.get("http://no.such.domain");

	// Will start all at first check
	BOOST_CHECK(r200.check(200, "Example Domain"));
	BOOST_CHECK(r200_1.check(200, "From The Collaborative International Dictionary of English"));
	BOOST_CHECK(r404.check(404, "404 Not Found"));

	BOOST_CHECK_EQUAL(rWrongUrl.fail(), "Couldn't resolve host name");
	BOOST_CHECK_EQUAL(rWrongDomain.fail(), "Couldn't resolve host name");
}

static Request::Headers headers() {
	return {
		"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:32.0) Gecko/20100101 Firefox/32.0",
		"Accept-Language: ru,en;q=0.8",
	};
}

BOOST_AUTO_TEST_CASE( withHeaders ) {
	Tester t;
	auto r200 = t.get("http://www.livejournal.com/search/?area=posts&how=tm&q=123", headers());
	BOOST_CHECK(r200.check(200, "livejournal.com"));
}

